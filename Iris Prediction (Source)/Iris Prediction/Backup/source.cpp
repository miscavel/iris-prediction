#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include "GL/glut.h"

float draw_scale_x = 0.7;
float draw_scale_y = 0.5;
float draw_offset_x = -5.0;
float draw_offset_y = -2.5;
float scale_x = 0.015;
float scale_y = 2.0;
float offset_x = -1.0;
float offset_y = -1.0;
float _x, _y;

class Iris
{
	std::vector<float> components; //0 = slength, 1 = swidth, 2 = plength, 3 = pwidth
	std::string type;
	bool plucked;
public:
	Iris(float slength = 0, float swidth = 0, float plength = 0, float pwidth = 0, std::string _type = "Iris-plateau")
	{
		components.push_back(slength);
		components.push_back(swidth);
		components.push_back(plength);
		components.push_back(pwidth);
		type = _type;
		plucked = false;
	}

	float GetComponent(int index)
	{
		return components[index];
	}

	int GetComponentSize()
	{
		return components.size();
	}

	void SetComponent(float value, int index)
	{
		components[index] = value;
	}

	void SetType(std::string _type)
	{
		type = _type;
	}

	std::string GetType()
	{
		return type;
	}

	void Pluck(bool status)
	{
		plucked = status;
	}

	bool IsPlucked()
	{
		return plucked;
	}
};

class Bouquet
{
	std::vector<Iris> flowers;
public:
	void AddFlower(Iris &flower)
	{
		flowers.push_back(flower);
	}

	void LoadFromFile(std::string filename)
	{
		Iris dummy;
		std::string line;
		std::ifstream rfile(filename);
		while (rfile >> line)
		{
			for (int i = 0; i < dummy.GetComponentSize(); i++)
			{
				dummy.SetComponent(std::stof(line.substr(0, line.find(','))), i);
				line.erase(line.begin() + 0, line.begin() + line.find(',') + 1);
			}
			dummy.SetType(line);
			flowers.push_back(dummy);
		}

		rfile.close();
	}

	void PrintAll()
	{
		for (int i = 0; i < flowers.size(); i++)
		{
			for (int j = 0; j < flowers[i].GetComponentSize(); j++)
			{
				std::cout << flowers[i].GetComponent(j) << "\t";
			}
			std::cout << flowers[i].GetType() << "\n";
		}
	}

	int GetFlowerCount()
	{
		return flowers.size();
	}

	Iris* GetFlower(int index)
	{
		return &flowers[index];
	}
};

float CalculateDistance(Iris *one, Iris *two)
{
	float distance = 0;
	for (int i = 0; i < one->GetComponentSize(); i++)
	{
		distance += (float)pow((float)(one->GetComponent(i) - two->GetComponent(i)), (float)2.0);
	}
	return pow((float)distance, (float)0.5);
}

void CountString(std::vector<int> &stringCount, std::vector<std::string> &stringArr, std::string find)
{
	for (int i = 0; i < stringArr.size(); i++)
	{
		if (stringArr[i] == find)
		{
			stringCount[i]++;
		}
	}
	stringArr.push_back(find);
	stringCount.push_back(0);
}

void SortStringDesc(std::vector<int> &stringCount, std::vector<std::string> &stringArr)
{
	int temp;
	std::string tempstr;
	for (int i = 0; i < stringCount.size(); i++)
	{
		for (int j = i; j < stringCount.size(); j++)
		{
			if (stringCount[j] > stringCount[i])
			{
				temp = stringCount[i];
				stringCount[i] = stringCount[j];
				stringCount[j] = temp;

				tempstr = stringArr[i];
				stringArr[i] = stringArr[j];
				stringArr[j] = tempstr;
			}
		}
	}
}

class Rochette
{
	Bouquet bouquet, bush;
	std::vector<std::vector<Iris*>> vase;
	std::vector<float> errors;
	std::vector<float> knnAverage;
	float distance;
	int req, k;

public:
	Rochette()
	{
		distance = 3.0;
		req = 3;
	}

	void SetBouquet(Bouquet &data)
	{
		bouquet = data;
		req = data.GetFlower(0)->GetComponentSize() + 1;
		k = req - 1;

		std::cout << "Dimension Size : " << (req - 1) << "\n";
		std::cout << "Min Points : " << req << "\n";
		std::cout << "K : " << (req - 1) << "\n";
	}

	void SetBush(Bouquet &data)
	{
		bush = data;
	}

	void IdentifyBush()
	{
		std::cout << "Identifying Bush..\n";
		std::ofstream wfile("predict.txt");

		std::vector<std::string> stringArr;
		std::vector<std::string> results;
		std::vector<int> stringCount;
		float min, cluster;
		for (int i = 0; i < bush.GetFlowerCount(); i++)
		{
			min = -1;
			std::cout << vase.size() << "\n";
			for (int j = 0; j < vase.size(); j++)
			{
				for (int k = 0; k < vase[j].size(); k++)
				{
					if (min == -1)
					{
						min = CalculateDistance(bush.GetFlower(i), vase[j][k]);
						cluster = j;
					}
					else
					{
						if (CalculateDistance(bush.GetFlower(i), vase[j][k]) < min)
						{
							min = CalculateDistance(bush.GetFlower(i), vase[j][k]);
							cluster = j;
						}
					}
				}
			}

			std::cout << "Cluster : " << cluster << "\n";

			for (int i = 0; i < vase[cluster].size(); i++)
			{
				CountString(stringCount, stringArr, vase[cluster][i]->GetType());
			}

			SortStringDesc(stringCount, stringArr);

			wfile << stringArr[0] << "\n";

			results.push_back(stringArr[0]);

			stringArr.clear();
			stringCount.clear();
		}

		wfile.close();

		std::cout << "Success!\n";

		IdentificationAccuracy(results);
	}

	void IdentificationAccuracy(std::vector<std::string> &results)
	{
		float accuracy = 0;
		for (int i = 0; i < bush.GetFlowerCount(); i++)
		{
			if (results[i] == bush.GetFlower(i)->GetType())
			{
				accuracy++;
			}
		}
		accuracy = (accuracy * 100) / bush.GetFlowerCount();
		std::cout << "Accuracy : " << accuracy << "%\n";
	}

	float AverageKNN(Iris *self)
	{
		std::vector<float> distanceArr;
		for (int i = 0; i < bouquet.GetFlowerCount(); i++)
		{
			if (bouquet.GetFlower(i) != self)
			{
				distanceArr.push_back(CalculateDistance(self, bouquet.GetFlower(i)));
			}
		}

		float temp;
		for (int i = 0; i < distanceArr.size(); i++)
		{
			for (int j = i; j < distanceArr.size(); j++)
			{
				if (distanceArr[j] < distanceArr[i])
				{
					temp = distanceArr[i];
					distanceArr[i] = distanceArr[j];
					distanceArr[j] = temp;
				}
			}
		}

		float average = 0;
		for (int i = 0; i < k; i++)
		{
			average += distanceArr[i];
		}

		return (average / k);
	}

	void DistanceGraphKNN()
	{
		std::cout << "Calculating Average KNN-Distance Graph..\n";
		for (int i = 0; i < bouquet.GetFlowerCount(); i++)
		{
			knnAverage.push_back(AverageKNN(bouquet.GetFlower(i)));
		}

		float temp;
		for (int i = 0; i < knnAverage.size(); i++)
		{
			for (int j = i; j < knnAverage.size(); j++)
			{
				if (knnAverage[j] < knnAverage[i])
				{
					temp = knnAverage[i];
					knnAverage[i] = knnAverage[j];
					knnAverage[j] = temp;
				}
			}
		}
	}

	void PlotKNNAverage()
	{
		std::cout << "Plotting Average KNN-Distance Graph..\n";
		glColor3f(0, 0, 0);
		glPointSize(3.0);
		glBegin(GL_POINTS);
		for (int i = 0; i < knnAverage.size(); i++)
		{
			glVertex3f((i * scale_x) + offset_x, (knnAverage[i] * scale_y) + offset_y, 0);
		}
		glEnd();
		std::cout << "Waiting for the user to choose the best point..\n";
	}

	void PlotClusters()
	{
		float x, y;
		for (int i = 0; i < vase.size(); i++)
		{
			glColor3f(0, 0, 0);
			glBegin(GL_POLYGON);
			for (int j = 0; j < vase[i].size(); j++)
			{
				x = 0;
				y = 0;
				for (int k = 0; k < (vase[i][j]->GetComponentSize() / 2); k++)
				{
					x += vase[i][j]->GetComponent(k);
				}

				x /= (vase[i][j]->GetComponentSize() / 2);

				for (int k = (vase[i][j]->GetComponentSize() / 2); k < vase[i][j]->GetComponentSize(); k++)
				{
					y += vase[i][j]->GetComponent(k);
				}

				y /= (vase[i][j]->GetComponentSize() / 2);

				glVertex3f((x + draw_offset_x) * draw_scale_x, (y + draw_offset_y) * draw_scale_y, 0);
			}
			glEnd();
		}
	}

	void DensityClustering()
	{
		std::cout << "Clustering..\n";
		std::vector<Iris*> stems;
		for (int i = 0; i < bouquet.GetFlowerCount(); i++)
		{
			if (!bouquet.GetFlower(i)->IsPlucked())
			{
				for (int j = 0; j < bouquet.GetFlowerCount(); j++)
				{
					if (CalculateDistance(bouquet.GetFlower(i), bouquet.GetFlower(j)) <= distance)
					{
						stems.push_back(bouquet.GetFlower(j));
					}
				}
				if (stems.size() >= req)
				{
					for (int k = 0; k < stems.size(); k++)
					{
						stems[k]->Pluck(true);
					}
					for (int k = 0; k < stems.size(); k++)
					{
						Sprout(stems[k], stems);
					}
					vase.push_back(stems);
				}
			}
			stems.clear();
		}
	}

	void Sprout(Iris* self, std::vector<Iris*> &stems)
	{
		std::vector<Iris*> temp;
		int plucked = 0;
		for (int i = 0; i < bouquet.GetFlowerCount(); i++)
		{
			if (CalculateDistance(self, bouquet.GetFlower(i)) <= distance)
			{
				if (!bouquet.GetFlower(i)->IsPlucked())
				{
					temp.push_back(bouquet.GetFlower(i));
				}
				plucked++;
			}
		}
		
		if (plucked >= req)
		{
			for (int i = 0; i < temp.size(); i++)
			{
				temp[i]->Pluck(true);
				stems.push_back(temp[i]);
			}
			for (int k = stems.size() - temp.size(); k < stems.size(); k++)
			{
				Sprout(stems[k], stems);
			}
		}
		temp.clear();
	}

	void WriteResults(std::string filename)
	{
		std::cout << "Writing to file..\n";
		std::ofstream wfile(filename);
		
		for (int i = 0; i < vase.size(); i++)
		{
			wfile << "Cluster - " << i << "\n";
			for (int j = 0; j < vase[i].size(); j++)
			{
				wfile << vase[i][j]->GetType() << "\n";
			}
			wfile << "\n-----\n";
		}

		wfile.close();
		std::cout << "Done!\n";
	}

	void SetDistance(float _distance)
	{
		distance = _distance;
	}
};

Bouquet iris, bush;
Rochette mk1;

void Render()
{
	glClearColor(255,255,255,1.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	mk1.PlotKNNAverage();
	glutSwapBuffers();
}

void Cluster()
{
	glClearColor(255,255,255,1.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	mk1.PlotClusters();
	glutSwapBuffers();
}

void Idle()
{
	
}

void MouseInput(int button, int state, int x, int y)
{
	if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN)
	{
		_x = (float)(x - (glutGet(GLUT_WINDOW_WIDTH) / 2)) / (float)(glutGet(GLUT_WINDOW_WIDTH) / 2);
		_y = (float)(glutGet(GLUT_WINDOW_HEIGHT) - y - (glutGet(GLUT_WINDOW_HEIGHT) / 2)) / (float)(glutGet(GLUT_WINDOW_HEIGHT) / 2);

		_x = (_x - offset_x) / scale_x;
		_y = (_y - offset_y) / scale_y;
		std::cout << "Chosen point : (" << _x << ", " << _y << ")\n";
	}
	else if (button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN)
	{
		mk1.SetDistance(_y);
		std::cout << "Selected " << _y << " as density radius\n";
		mk1.DensityClustering();
		mk1.WriteResults("results.txt");
	}
}

void KeyInput(unsigned char key, int x, int y)
{
	if (key == '1')
	{
		glutDisplayFunc(Render);
	}
	else if (key == '2')
	{
		glutDisplayFunc(Cluster);
		glutPostRedisplay();
	}
	else if (key == 'i')
	{
		mk1.IdentifyBush();
	}
}

void main(int argc, char **argv)
{
	iris.LoadFromFile("iris-train.csv");
	bush.LoadFromFile("iris-test.csv");
	
	mk1.SetBouquet(iris);
	mk1.SetBush(bush);
	mk1.DistanceGraphKNN();
	//mk1.DensityClustering();
	//mk1.WriteResults("result.txt");

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
	
	glutInitWindowSize(800,600);
	glutCreateWindow("DBSCAN");
	glutDisplayFunc(Render);

	glutMouseFunc(MouseInput);
	glutKeyboardFunc(KeyInput);
	//glutMotionFunc(HoldUpdate);
	glutIdleFunc(Idle);
	glutMainLoop();
}